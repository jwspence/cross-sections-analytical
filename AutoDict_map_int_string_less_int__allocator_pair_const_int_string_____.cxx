#include "LHAPDF-6.2.3/include/LHAPDF/LHAPDF.h"
#include "vector"
#include "string"
#ifdef __CINT__ 
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
#pragma link C++ class map<int,string,less<int>,allocator<pair<const int,string> > >+;
#pragma link C++ class map<int,string,less<int>,allocator<pair<const int,string> > >::*;
#pragma link C++ operators map<int,string,less<int>,allocator<pair<const int,string> > >::iterator;
#pragma link C++ operators map<int,string,less<int>,allocator<pair<const int,string> > >::const_iterator;
#pragma link C++ operators map<int,string,less<int>,allocator<pair<const int,string> > >::reverse_iterator;
#endif
