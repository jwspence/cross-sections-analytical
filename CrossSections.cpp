#include <cmath>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
#include "LHAPDF-6.2.3/include/LHAPDF/LHAPDF.h"
R__LOAD_LIBRARY(/afs/cern.ch/user/j/jwspence/WORK/cross-sections-analytical/LHAPDF-6.2.3/installed/lib/libLHAPDF.so);

//#include "Cross_Sections.h"
//#include "Constants.h"
//#include "Interpolate.h"

const double A_W = 183.84; // A for Tungsten
const double Z_W = 74.; // Z for Tungsten
const double N_W = A_W - Z_W; // N for Tungsten
const double mp = 0.93828;
const double GF  = 1.1663787e-5;
const double mW = 80.37;
const double mZ = 91.1876;
const double xW = 0.226;
const double Lusq = pow(1-4/3*xW,2);
const double Ldsq = pow(-1+2/3*xW,2);
const double Rusq = pow(-4/3*xW,2);
const double Rdsq = pow(2/3*xW,2);
// LHAPDF paper:1412.7420
// NNPDF 3.1 NNLO paper:1706.00428
// See 1803.07977 page 80 ref 598
LHAPDF::PDF* pdf = LHAPDF::mkPDF("NNPDF31_nnlo_as_0118", 0);
//LHAPDF::PDF* pdf = LHAPDF::mkPDF("cteq6", 0);

// eq. 9
double xq_CC(double x, double Qsq)
{
	double xq = 0;
	xq += Z_W * pdf->xfxQ2(1, x, Qsq); // for protons use the d quark
	xq += N_W * pdf->xfxQ2(2, x, Qsq); // for neutrons use the d quark which is given by the proton's pdf of the u quark by isoscalar conjugation
	xq += A_W * (pdf->xfxQ2(3, x, Qsq) + pdf->xfxQ2(5, x, Qsq));
	return xq;
}

// eq. 9
double xqbar_CC(double x, double Qsq)
{
	double xq = 0;
	xq += Z_W * pdf->xfxQ2(-2, x, Qsq); // for protons use the ubar quark
	xq += N_W * pdf->xfxQ2(-1, x, Qsq); // for neutrons use the ubar quark which is given by the proton's pdf of the dbar quark by isoscalar conjugation
	xq += A_W * (pdf->xfxQ2(4, x, Qsq) + pdf->xfxQ2(6, x, Qsq));
	return xq;
}

// eq. 8
double d2sigma_xy_CC(double Enu, double x, double y, bool nu)
{
	double m, a, b, c, d, Qsq;

	m = mp;
	Qsq = x * 2 * m * y * Enu;

	a = ((2 * pow(GF, 2) * m * Enu) / M_PI);
	b = pow(pow(mW, 2) / (Qsq + pow(mW, 2)), 2);
	if (nu)
		c = xq_CC(x, Qsq) + xqbar_CC(x, Qsq) * pow(1 - y, 2);
	else
		c = xq_CC(x, Qsq) * pow(1 - y, 2) + xqbar_CC(x, Qsq);
	d = 0.3894 * 1e-27; // GeV^-2 to cm^2
	return a * b * c * d;
}

// eq. 11
double xq_NC(double x, double Qsq)
{
	double xq = 0;
	xq += pdf->xfxQ2(2, x, Qsq) * (Z_W * Lusq + N_W * Ldsq); // u
	xq += pdf->xfxQ2(1, x, Qsq) * (Z_W * Ldsq + N_W * Lusq); // d
	xq += pdf->xfxQ2(-2, x, Qsq) * (Z_W * Rusq + N_W * Rdsq); // ubar
	xq += pdf->xfxQ2(-1, x, Qsq) * (Z_W * Rdsq + N_W * Rusq); // dbar
	xq += A_W * (Ldsq + Rdsq) * (pdf->xfxQ2(3, x, Qsq) + pdf->xfxQ2(5, x, Qsq)); // s, b
	xq += A_W * (Lusq + Rusq) * (pdf->xfxQ2(4, x, Qsq) + pdf->xfxQ2(6, x, Qsq)); // c, t
	return xq;
}

// eq. 11
double xqbar_NC(double x, double Qsq)
{
	double xq = 0;
	xq += pdf->xfxQ2(2, x, Qsq) * (Z_W * Rusq + N_W * Rdsq); // u
	xq += pdf->xfxQ2(1, x, Qsq) * (Z_W * Rdsq + N_W * Rusq); // d
	xq += pdf->xfxQ2(-2, x, Qsq) * (Z_W * Lusq + N_W * Ldsq); // ubar
	xq += pdf->xfxQ2(-1, x, Qsq) * (Z_W * Ldsq + N_W * Lusq); // dbar
	xq += A_W * (Ldsq + Rdsq) * (pdf->xfxQ2(3, x, Qsq) + pdf->xfxQ2(5, x, Qsq)); // s, b
	xq += A_W * (Lusq + Rusq) * (pdf->xfxQ2(4, x, Qsq) + pdf->xfxQ2(6, x, Qsq)); // c, t
	return xq;
}

// eq. 10
double d2sigma_xy_NC(double Enu, double x, double y, bool nu)
{
	double m, a, b, c, d, Qsq;

	m = mp;
	Qsq = x * 2 * m * y * Enu;

	a = ((pow(GF, 2) * m * Enu) / (2 * M_PI));
	b = pow(pow(mZ, 2) / (Qsq + pow(mZ, 2)), 2);
	if (nu)
		c = xq_NC(x, Qsq) + xqbar_NC(x, Qsq) * pow(1 - y, 2);
	else
		c = xq_NC(x, Qsq) * pow(1 - y, 2) + xqbar_NC(x, Qsq);
	d = 0.3894 * 1e-27; // GeV^-2 to cm^2
	return a * b * c * d;
}

double sigma(double Enu, bool nu, bool CC)
{
	double integral, x, y, logx, logy, logx_min, logy_min, logx_max, logy_max, dlogx, dlogy, p;
	int n_step;

	integral = 0;

	n_step = 1e3;

	logx_min = log(1e-4);
	logx_max = 0;
	dlogx = (logx_max - logx_min) / n_step;
        logy_min = log(1e-4);
        logy_max = 0;
        dlogy = (logy_max - logy_min) / n_step;

	# pragma omp parallel for private(x, p) reduction(+ : integral)
	for (int ix = 0; ix <= n_step; ix++)
	{
            for (int iy=0; iy <= n_step; iy++)
            {
		logx = logx_min + ix * dlogx;
		x = exp(logx);
                logy = logy_min + iy * dlogy;
                y = exp(logy);
		if (CC)
			p = d2sigma_xy_CC(Enu, x, y, nu);
		else
			p = d2sigma_xy_NC(Enu, x, y, nu);
		integral += p * x * y;
            } // iy
	} // ix
	integral *= dlogx * dlogy;

	return integral;
}

void CrossSections(){
     using namespace std;
    	ifstream in("nu_CC_john.txt", ifstream::in);
    	Double_t lstNumbers[102];
        int i = 0;
    	while((!in.eof()) && in)
    		{
    		float iNumber = 0;
                in >> iNumber;
    		lstNumbers[i] = iNumber;
                i += 1;
    		}
    	in.close();
   gSystem->Load("LHAPDF-6.2.3/installed/lib/libLHAPDF.so");
   TCanvas *c1 = new TCanvas("c1","Cross Sections for nu tau",10,1000,200,600);
   c1->SetLogx();
   c1->SetLogy();
   Double_t x[101], y[101],z[101];
   Int_t n = 101;
   for (Int_t i=0;i<n;i++) {
     x[i] = 10*pow(1000,i/100.);
     y[i] = sigma(x[i],1,1)/x[i];
     z[i] = lstNumbers[i]/x[i];
     cout << x[i] << " " << y[i] << " " << z[i] << endl;
   }
   TGraph* gr = new TGraph(n,x,y);
   TGraph* cmp = new TGraph(n,x,z);
   gr->SetTitle("Analytical formula");
   cmp->SetTitle("Reference cross sections");
   cmp->SetLineColor(4);
//   gr->Draw("AC*");
   TMultiGraph *mg = new TMultiGraph();
   mg->Add(gr);
   mg->Add(cmp);
   mg->Draw("a");
   mg->SetTitle("Cross Sections vs. Energy (Comparison)");
   mg->GetXaxis()->SetTitle("Energy (GeV)");
   mg->GetYaxis()->SetTitle("Total Cross Section (cm^2)");
   c1->BuildLegend();
}
