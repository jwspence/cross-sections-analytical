#include "vector"
class LHAPDF::PDF;
#ifdef __CINT__ 
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
#pragma link C++ class vector<LHAPDF::PDF*>+;
#pragma link C++ class vector<LHAPDF::PDF*>::*;
#ifdef G__VECTOR_HAS_CLASS_ITERATOR
#pragma link C++ operators vector<LHAPDF::PDF*>::iterator;
#pragma link C++ operators vector<LHAPDF::PDF*>::const_iterator;
#pragma link C++ operators vector<LHAPDF::PDF*>::reverse_iterator;
#endif
#endif
