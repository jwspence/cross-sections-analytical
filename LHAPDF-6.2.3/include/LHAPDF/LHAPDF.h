// -*- C++ -*-
//
// This file is part of LHAPDF
// Copyright (C) 2012-2019 The LHAPDF collaboration (see AUTHORS for details)
//
#pragma once
#ifndef LHAPDF_LHAPDF_H
#define LHAPDF_LHAPDF_H

/// @file LHAPDF.h
/// Just a convenience header that pulls in the main functionality with a single include

#include "/afs/cern.ch/user/j/jwspence/WORK/FASERnu1/LHAPDF/Version.h"
#include "/afs/cern.ch/user/j/jwspence/WORK/FASERnu1/LHAPDF/PDF.h"
#include "/afs/cern.ch/user/j/jwspence/WORK/FASERnu1/LHAPDF/PDFSet.h"
#include "/afs/cern.ch/user/j/jwspence/WORK/FASERnu1/LHAPDF/PDFInfo.h"
#include "/afs/cern.ch/user/j/jwspence/WORK/FASERnu1/LHAPDF/Factories.h"
#include "/afs/cern.ch/user/j/jwspence/WORK/FASERnu1/LHAPDF/PDFIndex.h"
#include "/afs/cern.ch/user/j/jwspence/WORK/FASERnu1/LHAPDF/Paths.h"
#include "/afs/cern.ch/user/j/jwspence/WORK/FASERnu1/LHAPDF/LHAGlue.h"

#endif
