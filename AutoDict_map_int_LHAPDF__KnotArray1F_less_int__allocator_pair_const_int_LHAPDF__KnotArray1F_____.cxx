#include "LHAPDF-6.2.3/include/LHAPDF/LHAPDF.h"
#include "vector"
#ifdef __CINT__ 
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
#pragma link C++ class map<int,LHAPDF::KnotArray1F,less<int>,allocator<pair<const int,LHAPDF::KnotArray1F> > >+;
#pragma link C++ class map<int,LHAPDF::KnotArray1F,less<int>,allocator<pair<const int,LHAPDF::KnotArray1F> > >::*;
#pragma link C++ operators map<int,LHAPDF::KnotArray1F,less<int>,allocator<pair<const int,LHAPDF::KnotArray1F> > >::iterator;
#pragma link C++ operators map<int,LHAPDF::KnotArray1F,less<int>,allocator<pair<const int,LHAPDF::KnotArray1F> > >::const_iterator;
#pragma link C++ operators map<int,LHAPDF::KnotArray1F,less<int>,allocator<pair<const int,LHAPDF::KnotArray1F> > >::reverse_iterator;
#endif
